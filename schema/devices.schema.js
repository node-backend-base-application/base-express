"use strict";

const { Joi } = require("express-validation");

module.exports = {
  addDeviceSchema() {
    return {
      body: Joi.object({
        device_id: Joi.number().required(),
        device_name: Joi.string().required(),
        public_name: Joi.string().required(),
        device_category_id: Joi.number().required(),
        dgca_registered_date: Joi.date().required(),
        status: Joi.string().required(),
        u_id: Joi.number().required(),
      }),
    };
  },
  getDeviceById() {
    return {
      params: Joi.object({
        device_id: Joi.number().required(),
      }),
    };
  },
};
