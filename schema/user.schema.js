"use strict";

const { Joi } = require("express-validation");

module.exports = {
  createUserSchema() {
    return {
      body: Joi.object({
        first_name: Joi.string(),
        last_name: Joi.string(),
        email: Joi.string().email().required(),
        password: Joi.string().required(),
      }),
    };
  },
  getUserById() {
    return {
      params: Joi.object({
        u_id: Joi.number().required(),
      }),
    };
  },
};
