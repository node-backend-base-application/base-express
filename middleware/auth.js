const jwt = require('jsonwebtoken');
require('dotenv').config();
const db = require('../models/db');
const Users = db.users;
const config = process.env;

const verifyToken = async (req, res, next) => {
  let token = null;
  if (req.headers.authorization) {
    const authHeader = req.headers.authorization;
    token = authHeader && authHeader.split(' ')[1];
  }

  if (req.body.token) {
    token = req.body.token;
  }

  if (!token) {
    return res.status(403).send('A token is required for authentication');
  }
  try {
    const decoded = jwt.verify(token, config.TOKEN_SECRET);
    const userDetails = await Users.findOne({
      where: { email: decoded.email, id: decoded.user_id },
    });
    if (userDetails) {
      req.user = decoded;
    } else {
      return res.status(401).send('Invalid Token');
    }
  } catch (err) {
    return res.status(401).send('Invalid Token');
  }
  return next();
};

module.exports = verifyToken;
