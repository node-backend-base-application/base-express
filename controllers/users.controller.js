const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
require('dotenv').config();
const db = require('../models/db');
const Users = db.users;

module.exports = {
  createUser (req, res) {
    // Create a Tutorial
    const user = {
      first_name: req.body.first_name,
      last_name: req.body.last_name,
      email: req.body.email,
      password: req.body.password,
    };

    // Save Tutorial in the database
    Users.create(user)
      .then((data) => {
        res.send(data);
      })
      .catch((err) => {
        res.status(500).send({
          message:
            err.message || 'Some error occurred while creating the User.',
        });
      });
  },
  register: async function (req, res) {
    try {
      // Create a Tutorial
      const { first_name, last_name, email, password } = req.body;

      if (!(email && password && first_name && last_name)) {
        res.status(400).send('All input is required');
      }

      const oldUser = await Users.findOne({ where: { email: email } });
      console.log(oldUser);

      if (oldUser) {
        return res.status(409).send('User Already Exist. Please Login');
      }

      // Encrypt user password
      const encryptedPassword = await bcrypt.hash(password, 10);

      // Create user in our database
      let user = await Users.create({
        first_name,
        last_name,
        email: email.toLowerCase(), // sanitize: convert email to lowercase
        password: encryptedPassword,
      });

      // Create token
      console.log(process.env.TOKEN_SECRET);
      const token = jwt.sign(
        { user_id: user.id, email },
        process.env.TOKEN_SECRET,
        {
          expiresIn: '2h',
        },
      );
      user = user.toJSON();
      // save user token
      user.token = token;

      res.status(201).json(user);
    } catch (err) {
      console.log(err);
    }
  },

  login: async function (req, res) {
    // Our login logic starts here
    try {
      // Get user input
      const { email, password } = req.body;

      // Validate user input
      if (!(email && password)) {
        res.status(400).send('All input is required');
      }
      // Validate if user exist in our database
      let user = await Users.findOne({ where: { email: email } });

      if (user && (await bcrypt.compare(password, user.password))) {
        // Create token
        const token = jwt.sign(
          { user_id: user.id, email },
          process.env.TOKEN_SECRET,
          {
            expiresIn: '2h',
          },
        );

        user = user.toJSON();
        // save user token
        user.token = token;

        // user
        res.status(200).json(user);
      }
      res.status(400).send('Invalid Credentials');
    } catch (err) {
      console.log(err);
    }
  },
  getAllUsers (req, res) {
    Users.findAll().then((users) => {
      res.send(users);
    });
  },
  async getUserById (req, res) {
    const { u_id } = req.params;
    try {
      const user = await Users.findOne({ where: { id: u_id } });
      res.json(user);
    } catch (error) {}
  },
};
