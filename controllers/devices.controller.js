const db = require('../models/db');
const Devices = db.devices;

module.exports = {
  async addDevice (req, res) {
    const {
      device_id,
      device_name,
      public_name,
      device_category_id,
      dgca_registered_date,
      status,
      u_id,
    } = req.body;

    const payload = {
      device_id: device_id,
      device_name: device_name,
      public_name: public_name,
      device_category_id: device_category_id,
      dgca_registered_date: dgca_registered_date,
      status: status,
      created_date: new Date(),
      created_by: u_id,
      edited_date: new Date(),
      edited_by: u_id,
    };
    try {
      const addDevice = await Devices.create(payload);
      res.send(addDevice);
    } catch (error) {
      console.log(error);
    }
  },
  async getDeviceById (req, res) {
    const { device_id } = req.params;
    try {
      const allUsersDetails = await Devices.findOne({
        where: { device_id: device_id },
      });
      res.send(allUsersDetails);
    } catch (error) {}
  },
};
