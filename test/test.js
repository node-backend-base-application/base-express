// Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();

chai.use(chaiHttp);
describe('Users', () => {
  /*
   * Test the /GET route
   */
  describe('/GET User', () => {
    it('it should GET all the User', (done) => {
      chai
        .request('http://localhost:3001')
        .get('/api/user/getAllUsers')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          // res.body.length.should.be.eql(1);
          done();
        });
    });
  });
});
