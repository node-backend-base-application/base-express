module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
  },
  extends: ['standard'],
  parserOptions: {
    ecmaVersion: 12,
  },
  rules: {
    camelcase: 0,
    'comma-dangle': [2, 'always-multiline'],
    quotes: [2, 'single', { avoidEscape: true }],
    semi: [2, 'always'],
  },
};
