const express = require('express');
const router = express.Router();
const verifyToken = require('../middleware/auth');
const { validate } = require('express-validation');
const DeviceSchema = require('../schema/devices.schema');

const DeviceController = require('../controllers/devices.controller');

router.post('/addDevice', DeviceController.addDevice);
router.get(
  '/getDeviceById/:device_id',
  validate(DeviceSchema.getDeviceById(), {}, {}),
  DeviceController.getDeviceById,
);

module.exports = router;
