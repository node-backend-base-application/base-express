const express = require('express');
const router = express.Router();
const { validate } = require('express-validation');
const verifyToken = require('../middleware/auth');

const UserController = require('../controllers/users.controller');
const UserSchema = require('../schema/user.schema');

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});
router.post(
  '/createUser',
  verifyToken,
  validate(UserSchema.createUserSchema(), {}, {}),
  UserController.createUser,
);
router.get('/getAllUsers', UserController.getAllUsers);
router.post(
  '/register',
  validate(UserSchema.createUserSchema(), {}, {}),
  UserController.register,
);
router.get(
  '/getUserById/:u_id',
  validate(UserSchema.getUserById(), {}, {}),
  UserController.getUserById,
);
router.post('/login', UserController.login);

module.exports = router;
